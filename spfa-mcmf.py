#!/usr/bin/env python3

from copy import deepcopy
from queue import PriorityQueue

# vertices are numbered from [0, vertex count)
# edges are stored in a matrix of edges, where (x, y) means the edge from x to y
# assumes (x, y) and (y, x) cannot coexist
class Graph:
    class Edge:
        def __init__(self, x, y, capacity, cost):
            self.x = x
            self.y = y
            self.capacity = capacity
            self.cost = cost
        def __str__(self):
            return '{}->{} capacity: {} cost: {}'.format(self.x, self.y, self.capacity, self.cost)
    def __init__(self, vertexCount, source, sink):
        assert(source < vertexCount and source >= 0)
        assert(sink < vertexCount and sink >= 0 and source != sink)

        self.vertexCount = vertexCount
        self.source = source
        self.sink = sink
        self.edges = [[Graph.Edge(x, y, 0, 0) for y in range(0, vertexCount)] for x in range(0, vertexCount)]
    # only called during initialization
    def SetEdge(self, a, b, capacity, cost):
        assert(self.edges[a][b].capacity == 0)
        assert(self.edges[a][b].cost == 0)
        assert(self.edges[b][a].capacity == 0)
        assert(self.edges[b][a].cost == 0)
        self.edges[a][b].capacity = capacity
        self.edges[a][b].cost = cost
        # add reverse edge with negative cost and no capacity
        self.edges[b][a].capacity = 0
        self.edges[b][a].cost = -cost

    def __str__(self):
        string = 'vertex count: {} source: {} sink: {}\n'.format(self.vertexCount, self.source, self.sink)
        for x, edgeRow in enumerate(self.edges):
            for y, edge in enumerate(edgeRow):
                if edge.capacity != 0 or edge.cost != 0:
                    string += str(edge) + '\n'
        return string

    def GetMinCostMaxFlow(self):
        graph = deepcopy(self.edges)
        def Augment():
            class VertexStates:
                def __init__(self, index):
                    self.index = index
                    # bottleneck capacity from source to current vertex
                    self.capacity = None
                    # cost of current path
                    self.cost = None
                    # last vertex of current path
                    self.last = None
                    # in the queue of path searching
                    self.inQueue = False
                def __lt__(self, other):
                    return self.cost < other.cost
            # init states for all vertices
            states = [VertexStates(x) for x in range(0, self.vertexCount)]
            states[self.source].cost = 0
            states[self.source].inQueue = True

            queue = PriorityQueue()
            queue.put(states[self.source])

            while not queue.empty():
                vertex = queue.get()
                vertex.inQueue = False
                x = vertex.index
                for y in range(0, self.vertexCount):
                    if graph[x][y].capacity > 0 and (not states[y].cost or states[x].cost + graph[x][y].cost < states[y].cost):
                        states[y].capacity = graph[x][y].capacity if not states[x].capacity else min(graph[x][y].capacity, states[x].capacity)
                        states[y].cost = states[x].cost + graph[x][y].cost
                        states[y].last = x
                        if not states[y].inQueue:
                            states[y].inQueue = True
                            queue.put(states[y])
            # should not use 'not last[self.sink]' here because 'not 0' is also true
            if states[self.sink].last == None:
                return 0, 0
            y = self.sink
            while y != self.source:
                x = states[y].last
                assert(x != None)
                graph[x][y].capacity -= states[self.sink].capacity
                graph[y][x].capacity += states[self.sink].capacity
                y = x
            return states[self.sink].capacity, states[self.sink].cost

        def GetFlowGraph():
            flows = []
            for x in range(0, self.vertexCount):
                for y in range(0, self.vertexCount):
                    if graph[x][y].cost < 0 and graph[x][y].capacity > 0:
                        flows.append(Graph.Edge(y, x, graph[x][y].capacity, -graph[x][y].cost))
            return flows
        totalCost = 0
        totalFlow = 0
        while True:
            flow, unitCost = Augment()
            if flow == 0:
                break
            totalCost += flow * unitCost
            totalFlow += flow
        return totalFlow, totalCost, GetFlowGraph()

# main
n, m = map(int, input().split())
graph = Graph(n, 0, n - 1)
for i in range(0, m):
    a, b, cap, cost = map(int, input().split())
    graph.SetEdge(a, b, cap, cost)
# print(graph)
maxflow, mincost, flows = graph.GetMinCostMaxFlow()
print(maxflow, mincost)
# for edge in flows:
#     print(edge)
