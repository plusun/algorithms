#!/usr/bin/env python3

import random

class Edge:
    def __init__(self, x, y, capacity, cost):
        self.x = x
        self.y = y
        self.capacity = capacity
        self.cost = cost
    def __str__(self):
        return '{} {} {} {}'.format(self.x, self.y, self.capacity, self.cost)

n = random.randint(2, 100)
edges = [[Edge(x, y, 0, 0) for y in range(0, n)] for x in range(0, n)]
m = 0
for y in range(0, n):
    for x in range(0, y):
        if random.randint(0, 4) == 3:
            continue
        m += 1
        edges[x][y].capacity = random.randint(1, 10000)
        edges[x][y].cost = random.randint(1, 10000)

print(n, m)
for x in range(0, n):
    for y in range(0, n):
        if edges[x][y].capacity != 0:
            print(x, y, edges[x][y].capacity, edges[x][y].cost)
