#include <cstdint>
#include <iostream>
#include <vector>
#include <queue>
#include <limits>

struct Edge
{
    // an edge connects vertices indexed with mA and mB
    Edge(uint64_t a, uint64_t b, uint64_t capacity, int64_t cost):
            mA(a), mB(b), mCapacity(capacity), mCost(cost) {}
    const uint64_t mA, mB;
    uint64_t mCapacity;
    const int64_t mCost;
    bool operator<(const Edge& other) { return mA < other.mA; }
    void SetReverse(Edge& edge) { mReverse = &edge; }
    Edge& GetReverse() { return *mReverse; }
  private:
    Edge* mReverse{nullptr};
};

struct Vertex
{
    // edges start with this vertex
    std::vector<std::unique_ptr<Edge>> mEdges;
};

typedef std::vector<Vertex> Graph;

Graph ReadGraph(void)
{
    uint64_t n, m;
    std::cin >> n >> m;

    Graph vertices(n);
    for (uint64_t i = 0; i < m; ++i)
    {
        uint64_t a, b, c;
        int64_t w;
        std::cin >> a >> b >> c >> w;
        assert(a != b);
        auto edge = std::unique_ptr<Edge>(new Edge(a, b, c, w));
        auto reverse = std::unique_ptr<Edge>(new Edge(b, a, 0, -w));
        edge->SetReverse(*reverse);
        reverse->SetReverse(*edge);
        vertices[a].mEdges.push_back(std::move(edge));
        vertices[b].mEdges.push_back(std::move(reverse));
    }
    return vertices;
}

void PrintGraph(const Graph& graph)
{
    std::cout << "Vertex count: " << graph.size() << std::endl;
    for (const Vertex& vertex: graph)
    {
        for (const auto& edge: vertex.mEdges)
        {
            std::cout << "Edge: " << edge.get() << " " << edge->mA << " " << edge->mB << " " << edge->mCapacity << " " << edge->mCost;
            std::cout << std::endl;
        }
    }
}

struct Solution
{
    uint64_t mMaxFlow{0}, mMinCost{0};
    Graph mFlows;
};

std::pair<uint64_t, uint64_t> Augment(Graph& graph, uint64_t source, uint64_t sink)
{
    std::vector<int64_t> cost(graph.size(), std::numeric_limits<int64_t>::max());
    std::vector<uint64_t> capacity(graph.size(), std::numeric_limits<uint64_t>::max());
    std::vector<uint64_t> last(graph.size(), sink);
    std::vector<bool> isVisited(graph.size(), false);
    std::queue<uint64_t> queue;

    queue.emplace(source);
    isVisited[source] = true;
    cost[source] = 0;

    while (!queue.empty())
    {
        auto current = queue.front();
        queue.pop();
        if (current == sink)
        {
            break;
        }
        for (const auto& edge: graph[current].mEdges)
        {
            if (edge->mCapacity > 0 && cost[edge->mA] + edge->mCost < cost[edge->mB])
            {
                cost[edge->mB] = cost[edge->mA] + edge->mCost;
                capacity[edge->mB] = std::min(capacity[edge->mA], edge->mCapacity);
                last[edge->mB] = edge->mA;
                if (!isVisited[edge->mB])
                {
                    isVisited[edge->mB] = true;
                    queue.emplace(edge->mB);
                }
            }
        }
    }

    if (last[sink] == sink)
    {
        return std::make_pair(0, 0);
    }

    for (auto current = sink; current != source; current = last[current])
    {
        // last[current]->current edge
        auto a{last[current]}, b{current};
        for (auto& edge: graph[a].mEdges)
        {
            if (edge->mB == b && edge->mCapacity >= capacity[sink])
            {
                edge->mCapacity -= capacity[sink];
                edge->GetReverse().mCapacity += capacity[sink];
                break;
            }
        }
    }

    return std::make_pair(capacity[sink], cost[sink]);
}

// assume cost is positive before EdmondsKarp
Graph GetFlowGraph(const Graph& graph)
{
    Graph flows(graph.size());
    for (auto& vertex: graph)
    {
        for (auto& edge: vertex.mEdges)
        {
            if (edge->mCost < 0 && edge->mCapacity > 0)
            {
                flows[edge->mB].mEdges.emplace_back(new Edge(edge->mB, edge->mA, edge->mCapacity, -edge->mCost));
            }
        }
    }
    return flows;
}

Solution EdmondsKarp(Graph &graph, uint64_t source, uint64_t sink)
{
    Solution solution;
    uint64_t maxFlow, minCost;
    do
    {
        std::tie(maxFlow, minCost) = Augment(graph, source, sink);
        solution.mMaxFlow += maxFlow;
        solution.mMinCost += minCost;
    }
    while (maxFlow != 0);
    solution.mFlows = GetFlowGraph(graph);
    PrintGraph(graph);
    PrintGraph(solution.mFlows);
    return solution;
}

int main(void)
{
    auto graph = ReadGraph();
    auto solution = EdmondsKarp(graph, 0, graph.size() - 1);
    std::cout << "Solution:" << std::endl << solution.mMaxFlow << " " << solution.mMinCost << std::endl;
    return 0;
}
