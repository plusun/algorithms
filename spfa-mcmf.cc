#include <iostream>
#include <vector>
#include <queue>

typedef long long uint64;
typedef long long int64;
const uint64 INF = 1 << 30;

struct Edge
{
    Edge(): capacity(0), cost(0) {}

    uint64 capacity;
    int64 cost;
};

struct Graph
{
    Graph(uint64 n, uint64 s, uint64 t): G(n, std::vector<Edge>(n)), Expect(0), S(s), T(t) {}
    std::vector<std::vector<Edge> > G;
    uint64 Expect, S, T;
};

class Index
{
  public:
    Index(uint64 n, uint64 m):
            mN(n), mM(m) {}
    uint64 S() const { return 0; } // 0
    uint64 M(uint64 i) const { return S() + 1 + i; } // 1->mM
    uint64 N(uint64 i) const { return M(mM) + i; } // mM+1->mN+mM
    uint64 T() const { return N(mN); } // mN+mM+1
    uint64 V() const { return T() + 1; };
  private:
    const uint64 mN, mM;
};

void AddEdge(std::vector< std::vector<Edge> >& graph, uint64 a, uint64 b, uint64 cap, int64 cost)
{
    graph[a][b].cost = cost;
    graph[a][b].capacity = cap;
    graph[b][a].cost = -cost;
    graph[b][a].capacity = 0;
}

Graph ReadGraph()
{
    uint64 n, m;
    std::cin >> n >> m;
    Graph graph(n, 0, n - 1);

    for (uint64 i = 0; i < m; ++i)
    {
        uint64 a, b, cap; int64 cost;
        std::cin >> a >> b >> cap >> cost;
        AddEdge(graph.G, a, b, cap, cost);
    }

    return graph;
}

std::pair<uint64, uint64> Augment(std::vector< std::vector<Edge> >& g, uint64 s, uint64 t)
{
    uint64 n = g.size();
    std::vector<uint64> cap(n, INF), last(n, n + 1);
    std::vector<int64> cost(n, INF);
    std::vector<bool> inQueue(n, false);
    std::queue<uint64> queue;
    cost[s] = 0;
    inQueue[s] = true;
    queue.push(s);

    while (!queue.empty())
    {
        uint64 cur = queue.front();
        queue.pop();
        inQueue[cur] = false;
        for (uint64 i = 0; i < n; ++i)
        {
            if (i == cur)
            {
                continue;
            }
            if (g[cur][i].capacity > 0 && g[cur][i].cost + cost[cur] < cost[i])
            {
                cap[i] = std::min(cap[cur], g[cur][i].capacity);
                cost[i] = g[cur][i].cost + cost[cur];
                last[i] = cur;
                if (!inQueue[i])
                {
                    inQueue[i] = true;
                    queue.push(i);
                }
            }
        }
    }
    if (last[t] == n + 1)
    {
        return std::make_pair(0, 0);
    }

    for (uint64 v = t; v != s; v = last[v])
    {
        g[last[v]][v].capacity -= cap[t];
        g[v][last[v]].capacity += cap[t];
    }
    return std::make_pair(cap[t], cost[t]);
}

std::pair<uint64, uint64> GetMinCostMaxFlow(std::vector< std::vector<Edge> > g, uint64 s, uint64 t)
{
    std::pair<uint64, uint64> pair;
    uint64 cost(0), cap(0);
    do
    {
        pair = Augment(g, s, t);
        cap += pair.first;
        cost += pair.first * pair.second;
    } while (pair.first != 0);
    return std::make_pair(cap, cost);
}

bool RunOnce()
{
    Graph g = ReadGraph();
    std::pair<uint64, uint64> result = GetMinCostMaxFlow(g.G, g.S, g.T);
    std::cout << result.first << ' ' << result.second << std::endl;
    return false;
}

int main()
{
    while (RunOnce()) {}

    return 0;
}
