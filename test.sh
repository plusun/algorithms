#!/bin/bash

while true; do
./graph-generator.py > input
time ./a.out < input > cc.output
time ./spfa-mcmf.py < input > py.output

if diff cc.output py.output > /dev/null; then
    cat py.output
    cat cc.output
else
    break
fi
done
